package io.cl.example.service;

import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;

@MicronautTest
public class ProductClientTest {

    @Inject
    ProductClient productClient;

    @Test
    void send() {
        productClient.send("Konichiwa".getBytes());
    }
}

package io.cl.example.service;

import io.micronaut.rabbitmq.annotation.Queue;
import io.micronaut.rabbitmq.annotation.RabbitListener;
import io.micronaut.rabbitmq.bind.RabbitAcknowledgement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RabbitListener
public class ProductListener {

    private static Logger logger = LoggerFactory.getLogger(ProductListener.class);

    /**
     * 消费者手动确认消息
     *
     * @param data 消息体
     * @param acknowledgement "确认"或"拒绝"消息的控制器
     */
    @Queue(value = "product.queue")
    public void normalReceive(byte[] data, RabbitAcknowledgement acknowledgement) {
        String dataString = new String(data);

        // 如果消息体为"ok", 正常消费并被确认
        if (dataString.equals("ok")) {
            acknowledgement.ack(false);
            logger.info("Queue 'product.queue' ACK Message is: {}", dataString);
        } else {
            // 否则消息被拒绝, 不会被重新放入队列, 最后进入死信队列
            acknowledgement.nack(false, false);
            logger.info("Queue 'product.queue' NACK Message is: {}", dataString);
        }
    }

    @Queue(value = "product.dlx.queue")
    public void deadLetterReceive(byte[] data, RabbitAcknowledgement acknowledgement) {
        String dataString = new String(data);

        // 消息进入死信队列, 消费并被确认
        acknowledgement.ack(false);
        logger.info("Queue 'product.dlx.queue' ACK Message is: {}", dataString);
    }

}

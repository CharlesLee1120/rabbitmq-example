package io.cl.example.service;

import io.micronaut.rabbitmq.annotation.Binding;
import io.micronaut.rabbitmq.annotation.RabbitClient;
import org.reactivestreams.Publisher;

import java.util.concurrent.CompletableFuture;

@RabbitClient
public interface ProductClient {

    @Binding("product.queue")
    void send(byte[] data);

    @Binding("product.queue")
    Publisher<Void> sendPublisher(byte[] data);

    @Binding("product.queue")
    CompletableFuture<Void> sendFuture(byte[] data);

}

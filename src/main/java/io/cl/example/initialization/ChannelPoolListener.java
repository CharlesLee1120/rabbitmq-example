package io.cl.example.initialization;

import com.rabbitmq.client.Channel;
import io.micronaut.rabbitmq.connect.ChannelInitializer;
import jakarta.inject.Singleton;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Singleton
public class ChannelPoolListener extends ChannelInitializer {

    @Override
    public void initialize(Channel channel, String name) throws IOException {

        String normalExchangeName = "product.direct";
        String deadLetterExchangeName = "product.dlx.direct";
        String queueName = "product.queue";
        String deadLetterQueueName = "product.dlx.queue";
        String routingKey = "routing.key";

        // 声明一个普通交换机
        channel.exchangeDeclare(normalExchangeName, "direct", false);

        // 声明一个死信交换机
        channel.exchangeDeclare(deadLetterExchangeName, "direct", false);

        // 声明一个普通队列
        Map<String, Object> args0 = new HashMap<>();
        args0.put("x-max-priority", 100);
        args0.put("x-message-ttl", 5000);
        // 设置死信交换机
        args0.put("x-dead-letter-exchange", deadLetterExchangeName);
        args0.put("x-dead-letter-routing-key", routingKey);

        channel.queueDeclare(queueName, false, false, false, args0);

        // 声明一个死信队列
        Map<String, Object> args1 = new HashMap<>();
        channel.queueDeclare(deadLetterQueueName, false, false, false, args1);

        // 绑定队列与交换机
        channel.queueBind(queueName, normalExchangeName, routingKey);
        channel.queueBind(deadLetterQueueName, deadLetterExchangeName, routingKey);

    }

}
